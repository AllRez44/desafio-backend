# Desafio Back-End  -  Allan Resende 
---
## App de Pagamento com Autenticação Externa - Projeto 'SuPaPO' (Super Payment for Purchase Order System)
---
### Front-End Design
![Desktop Landing Page's Design](/app_design/Landing_page_desktop.png)  
** <p> Work in Progress... No [Figma](https://www.figma.com/file/2KG39WiDW4DN6IRbPmlath/SuPaPo-System?node-id=0%3A1). </p>
---

### API / Archtechture Design

![Archtechture / API / Data Modeling](/app_design/archtecture_app_design.png)
** <p> Baseado no Design pattern "Mediator" e feito no site [DrawSQL](https://drawsql.app/teams/a-team-10/diagrams/supapo-system-diagram/embed) </p>

### Ferramentas Escolhidas

#### Back-End: Laravel 9 + NgInx
##### Porque Laravel?  

<p> Laravel já vem com ferramentas que adicionam várias possibilidades que facilitam o desenvolvimento, como Composer e Artisan. Além de também já vir com o framework PHPUnit para realização de testes. </p>

##### 
##### Front-End: Laravel também!???

###### Porque Laravel?

<p> Minha ideia principal era fazer o front-end com Vue.Js, devido à facilidade do framework e a vasta porém, devido a problemas de vunerabilidade recentes, tive que me adaptar as pressas. Por sorte, o Laravel possui diversas ferramentas, frameworks e bundlers através do que facilitam. Como Blaze e o Vite, que facilita ainda mais na criação com Frameworks e Libs JavaScript, exemplo React. </p>

<p> Exemplo: </p>

<img src="https://laravel.com/img/docs/breeze-register.png" max-width="70%"/>

---

### Demais Ferramentas Utilizadas:

#### Docker para separar o back-end em microsserviços

#### MySQL para Banco de Dados, containerizado, também em Docker

#### PHP puro.
